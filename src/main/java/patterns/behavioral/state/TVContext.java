package patterns.behavioral.state;

public class TVContext implements State {

    private State tvSate;

    public void setState(State tvSate) {
        this.tvSate = tvSate;
    }

    public State getState() {
        return this.tvSate;
    }

    @Override
    public void doAction() {
        this.tvSate.doAction();
    }
}
