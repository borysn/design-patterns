package patterns.behavioral.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TVOffState implements State {

    private static final Logger logger = LoggerFactory.getLogger(TVOffState.class);

    @Override
    public void doAction() {
        logger.info("TV is turned OFF");
    }
}
