package patterns.behavioral.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TVOnState implements State {

    private static final Logger logger = LoggerFactory.getLogger(TVOnState.class);

    @Override
    public void doAction() {
        logger.info("TV is turned ON");
    }
}
