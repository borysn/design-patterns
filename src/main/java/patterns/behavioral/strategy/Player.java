package patterns.behavioral.strategy;

public class Player {

    private Strategy strategy;

    public Player(Strategy strategy) {
        this.strategy = strategy;
    }

    public Strategy getStrategy() {
        return this.strategy;
    }

}
