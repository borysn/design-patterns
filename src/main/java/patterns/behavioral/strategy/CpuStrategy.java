package patterns.behavioral.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CpuStrategy implements Strategy {

    private static final Logger logger = LoggerFactory.getLogger(CpuStrategy.class);

    @Override
    public void makeMove() {
        logger.info("makeMove() | CpuStrategy");
    }
}
