package patterns.behavioral.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlayerStrategy implements Strategy {

    private static final Logger logger = LoggerFactory.getLogger(PlayerStrategy.class);

    @Override
    public void makeMove() {
        logger.info("makeMove() | PlayerStrategy");
    }
}
