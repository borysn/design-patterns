package patterns.behavioral.strategy;

import java.util.LinkedHashSet;
import java.util.Set;

public class Game {

    private Set<Player> players;

    public Game() {
        this.players = new LinkedHashSet<>();

        Player p1 = new Player(new PlayerStrategy());
        Player p2 = new Player(new CpuStrategy());

        this.players.add(p1);
        this.players.add(p2);
    }

    public void start() {
        this.players.forEach(p -> p.getStrategy().makeMove());
    }

}
