package patterns.behavioral.observer;

public interface SubjectIntf {

    void register(Observer o);
    void unregister(Observer o);
    void notifyObservers();

}
