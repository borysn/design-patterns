package patterns.behavioral.observer;

import java.util.LinkedHashSet;
import java.util.Set;

public class Subject implements SubjectIntf {

    private Set<Observer> observers;
    private int data;

    public Subject() {
        this.observers = new LinkedHashSet<>();
        data = 0;
    }

    public int getData() {
        return this.data;
    }

    public void setData(int data) {
        this.data = data;
        this.notifyObservers();
    }

    @Override
    public void register(Observer o) {
        this.observers.add(o);
    }

    @Override
    public void unregister(Observer o) {
        this.observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        this.observers.forEach(o -> o.update());
    }
}
