package patterns.behavioral.observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Observer {

    public static final Logger logger = LoggerFactory.getLogger(Observer.class);

    public void update() {
        logger.info("Subject data updated");
    }

}
