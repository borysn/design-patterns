package patterns.behavioral.chainofresponsibility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TwentyDollarDispenser implements DispenseChain {

    private static final Logger logger = LoggerFactory.getLogger(TwentyDollarDispenser.class);

    private DispenseChain nextChain;

    @Override
    public void setNextChain(DispenseChain nextChain) {
        this.nextChain = nextChain;
    }

    @Override
    public void dispense(Currency currency) {
        if (currency.getAmount() >= 20) {
            int num = currency.getAmount()/20;
            int remainder = currency.getAmount() % 20;

            logger.info("Dispensing " + num + " 20$ note(s)");

            if (remainder != 0) {
                this.nextChain.dispense(new Currency(remainder));
            }
        } else {
            this.nextChain.dispense(currency);
        }
    }
}
