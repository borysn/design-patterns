package patterns.behavioral.chainofresponsibility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FiftyDollarDispenser implements DispenseChain {

    private static final Logger logger = LoggerFactory.getLogger(FiftyDollarDispenser.class);

    private DispenseChain nextChain;

    @Override
    public void setNextChain(DispenseChain nextChain) {
        this.nextChain = nextChain;
    }

    @Override
    public void dispense(Currency currency) {

        logger.info("[Chain starting with: $" + currency.getAmount() + "]");

        if (currency.getAmount() >= 50) {
            int num = currency.getAmount()/50;
            int remainder = currency.getAmount() % 50;

            logger.info("Dispensing " + num + " 50$ note(s)");

            if (remainder != 0) {
                this.nextChain.dispense(new Currency(remainder));
            }
        } else {
            this.nextChain.dispense(currency);
        }
    }
}
