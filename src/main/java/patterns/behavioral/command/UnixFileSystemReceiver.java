package patterns.behavioral.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UnixFileSystemReceiver implements FileSystemReceiver {

    private static final Logger logger = LoggerFactory.getLogger(UnixFileSystemReceiver.class);

    @Override
    public void openFile() {
        logger.info("Opening file in unix OS");
    }

    @Override
    public void writeFile() {
        logger.info("Writing file in unix OS");
    }

    @Override
    public void closeFile() {
        logger.info("Closing file in unix OS");
    }
}
