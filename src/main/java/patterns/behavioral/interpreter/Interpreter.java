package patterns.behavioral.interpreter;

public class Interpreter {

    public InterpreterContext interpreterContext;

    public Interpreter(InterpreterContext interpreterContext) {
        this.interpreterContext = interpreterContext;
    }

    public String interpret(String expression) {
        Expression exp = null;

        if (expression.contains("Hexadecimal")) {
            exp = new IntToHexExpression(Integer.parseInt(
               expression.substring(0, expression.indexOf(" "))
            ));
        } else if (expression.contains("Binary")) {
            exp = new IntToBinaryExpression(Integer.parseInt(
                    expression.substring(0, expression.indexOf(" "))
            ));
        } else {
            return expression;
        }

        return exp.interpret(interpreterContext);
    }

}
