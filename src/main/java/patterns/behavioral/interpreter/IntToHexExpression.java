package patterns.behavioral.interpreter;

public class IntToHexExpression implements Expression {

    private int num;

    public IntToHexExpression(int num) {
        this.num = num;
    }

    @Override
    public String interpret(InterpreterContext interpreterContext) {
        return interpreterContext.getHexadecimalFormat(num);
    }
}
