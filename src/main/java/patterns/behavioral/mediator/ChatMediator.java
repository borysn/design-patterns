package patterns.behavioral.mediator;

import java.util.ArrayList;
import java.util.List;

public class ChatMediator implements ChatMediatorIntf {

    private List<User> users;

    public ChatMediator() {
        this.users = new ArrayList<>();
    }

    @Override
    public void sendMessage(String msg, User user) {
        users.forEach(u -> {
            if (u != user) {
                u.receive(msg);
            }
        });
    }

    @Override
    public void addUser(User user) {
        users.add(user);
    }
}
