package patterns.behavioral.mediator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class User implements UserIntf {

    private static final Logger logger = LoggerFactory.getLogger(User.class);

    protected ChatMediator mediator;
    protected String name;

    public User(ChatMediator mediator, String name) {
        this.mediator = mediator;
        this.name = name;
    }

    @Override
    public void send(String msg) {
        mediator.sendMessage(msg, this);
    }

    @Override
    public void receive(String msg) {
        logger.info("user [" + name + "] received message: \n\t" + msg);
    }
}
