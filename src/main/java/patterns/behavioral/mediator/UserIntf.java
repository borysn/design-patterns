package patterns.behavioral.mediator;

public interface UserIntf {

    public abstract void send(String msg);
    public abstract void receive(String msg);

}
