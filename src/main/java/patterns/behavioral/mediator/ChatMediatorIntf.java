package patterns.behavioral.mediator;

public interface ChatMediatorIntf {

    void sendMessage(String msg, User user);
    void addUser(User user);

}
