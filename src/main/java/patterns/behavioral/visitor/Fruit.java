package patterns.behavioral.visitor;

public class Fruit implements ItemElement {

    private int pricePerLb;
    private int weight;
    private String name;

    public Fruit(int pricePerLb, int weight, String name) {
        this.pricePerLb = pricePerLb;
        this.weight = weight;
        this.name = name;
    }

    public int getPricePerLb() {
        return this.pricePerLb;
    }

    public int getWeight() {
        return this.weight;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int accept(ShoppingCartVisitorIntf visitor) {
        return visitor.visit(this);
    }
}
