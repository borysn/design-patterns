package patterns.behavioral.visitor;

public interface ShoppingCartVisitorIntf {

    int visit(Book book);
    int visit(Fruit fruit);

}
