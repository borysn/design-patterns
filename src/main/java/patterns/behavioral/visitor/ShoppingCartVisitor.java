package patterns.behavioral.visitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShoppingCartVisitor implements ShoppingCartVisitorIntf {

    private static final Logger logger = LoggerFactory.getLogger(ShoppingCartVisitor.class);

    @Override
    public int visit(Book book) {
        int cost;

        // apply $5 discount if b ook price is greater than 50
        if (book.getPrice() > 50) {
            cost = book.getPrice() - 5;
        } else {
            cost = book.getPrice();
        }

        logger.info("Book ISBN::" + book.getIsbnNumber() + " cost: " + cost);

        return cost;
    }

    @Override
    public int visit(Fruit fruit) {
        int cost = fruit.getPricePerLb() * fruit.getWeight();

        logger.info("fruit::" + fruit.getName() + " cost: " + cost);

        return cost;
    }
}
