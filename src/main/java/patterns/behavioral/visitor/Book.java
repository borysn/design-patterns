package patterns.behavioral.visitor;

public class Book implements ItemElement {

    private int price;
    private String isbnNumber;

    public Book(int price, String isbnNumber) {
        this.price = price;
        this.isbnNumber = isbnNumber;
    }

    public int getPrice() {
        return price;
    }

    public String getIsbnNumber() {
        return this.isbnNumber;
    }

    @Override
    public int accept(ShoppingCartVisitorIntf visitor) {
        return visitor.visit(this);
    }
}
