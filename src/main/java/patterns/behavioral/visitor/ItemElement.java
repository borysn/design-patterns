package patterns.behavioral.visitor;

public interface ItemElement {

    int accept(ShoppingCartVisitorIntf visitor);

}
