package patterns.behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

public class Channels implements ChannelsIntf {

    private List<Channel> channels;

    public Channels() {
        channels = new ArrayList<>();
    }

    @Override
    public void addChannel(Channel channel) {
        channels.add(channel);
    }

    @Override
    public void removeChannel(Channel channel) {
        channels.remove(channel);
    }

    @Override
    public ChannelIteratorIntf iterator(ChannelType type) {
        return new ChannelIterator(type, channels);
    }
}
