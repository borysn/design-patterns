package patterns.behavioral.iterator;

public interface ChannelsIntf {

    void addChannel(Channel channel);
    void removeChannel(Channel channel);
    ChannelIteratorIntf iterator(ChannelType type);

}
