package patterns.behavioral.iterator;

public enum ChannelType {

    ENGLISH, HINDI, FRENCH, ALL;

}
