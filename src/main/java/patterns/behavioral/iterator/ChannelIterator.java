package patterns.behavioral.iterator;

import java.util.List;

public class ChannelIterator implements ChannelIteratorIntf {

    private ChannelType type;
    private List<Channel> channels;
    private int position;

    public ChannelIterator(ChannelType type, List<Channel> channels) {
        this.type = type;
        this.channels = channels;
    }

    @Override
    public boolean hasNext() {
        while (position < channels.size()) {
            Channel c = channels.get(position);
            if (c.getType().equals(type) || type.equals(ChannelType.ALL)) {
                return true;
            } else {
                position++;
            }
        }

        return false;
    }

    @Override
    public Channel next() {
        Channel channel = channels.get(position);
        position++;
        return channel;
    }
}
