package patterns.behavioral.iterator;

public interface ChannelIteratorIntf {

    boolean hasNext();
    public Channel next();

}
