package patterns.behavioral.iterator;

public class Channel {

    private double frequency;
    private ChannelType type;

    public Channel(double frequency, ChannelType type) {
        this.frequency = frequency;
        this.type = type;
    }

    public double getFrequency() {
        return frequency;
    }

    public ChannelType getType() {
        return this.type;
    }

    @Override
    public String toString() {
        return "Frequency: " + this.frequency + ", type: " + this.type;
    }

}
