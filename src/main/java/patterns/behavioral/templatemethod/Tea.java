package patterns.behavioral.templatemethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Tea extends BasicDrink {

    private static final Logger logger = LoggerFactory.getLogger(Tea.class);

    @Override
    public void addSpeciality() {
        logger.info("addSpecialty() | Tea");
    }
}
