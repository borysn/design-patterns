package patterns.behavioral.templatemethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Coffee extends BasicDrink {

    private static final Logger logger = LoggerFactory.getLogger(Coffee.class);

    @Override
    public void addSpeciality() {
        logger.info("addSpeciality() | Coffee");
    }
}
