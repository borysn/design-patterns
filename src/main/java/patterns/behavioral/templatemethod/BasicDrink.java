package patterns.behavioral.templatemethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BasicDrink {

    private static final Logger logger = LoggerFactory.getLogger(BasicDrink.class);

    public void makeDrink() {
        this.chooseCup();
        this.addSpeciality();
        this.chooseSweetner();
    }

    private void chooseCup() {
        logger.info("chooseCup() | BasicDrink");
    }

    private void chooseSweetner() {
        logger.info("chooseSweetner() | BasicDrink");
    }

    public abstract void addSpeciality();

}
