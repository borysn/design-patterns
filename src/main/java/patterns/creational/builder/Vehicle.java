package patterns.creational.builder;

public class Vehicle {

    private int wheels;
    private int passengers;
    private String engine;

    private boolean hasBlueTooth;
    private boolean hasAutomaticLocks;

    public int getWheels() {
        return this.wheels;
    }

    public int getPassengers() {
        return this.passengers;
    }

    public String getEngine() {
        return this.engine;
    }

    public boolean hasBlueTooth() {
        return this.hasBlueTooth;
    }

    public boolean hasAutomaticLocks() {
        return this.hasAutomaticLocks;
    }

    private Vehicle(VehicleBuilder builder) {
        this.wheels = builder.wheels;
        this.passengers = builder.passengers;
        this.engine = builder.engine;
        this.hasAutomaticLocks = builder.hasAutomaticLocks;
        this.hasBlueTooth = builder.hasBlueTooth;
    }

    public static class VehicleBuilder {

        private int wheels;
        private int passengers;
        private String engine;

        private boolean hasBlueTooth;
        private boolean hasAutomaticLocks;

        public VehicleBuilder(int wheels, int passengers, String engine) {
            this.wheels = wheels;
            this.passengers = passengers;
            this.engine = engine;
        }

        public VehicleBuilder setHasBlueTooth(boolean hasBlueTooth) {
            this.hasBlueTooth = hasBlueTooth;
            return this;
        }

        public VehicleBuilder setHasAutomaticLocks(boolean hasAutomaticLocks) {
            this.hasAutomaticLocks = hasAutomaticLocks;
            return this;
        }

        public Vehicle build() {
            return new Vehicle(this);
        }

    }

}
