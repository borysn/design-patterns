package patterns.creational.singleton;

public class Singleton {

    private static Singleton _singleton;

    private Singleton() {}

    private static class SingletonHelper {
        private static final Singleton _singleton = new Singleton();
    }

    public static Singleton getSingleton() {
        return SingletonHelper._singleton;
    }

}
