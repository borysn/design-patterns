package patterns.creational.prototype;

import java.util.ArrayList;
import java.util.List;

public class Employees implements Cloneable {

    private List<String> employeeList;

    public Employees() {
        this.employeeList = new ArrayList<>();
    }

    public Employees(List<String> employeeList) {
        this.employeeList = employeeList;
    }

    public void loadData() {
        employeeList.add("Borys");
        employeeList.add("David");
        employeeList.add("Isabelle");
        employeeList.add("Anita");
        employeeList.add("Roman");
    }

    public List<String> getEmployeeList() {
        return this.employeeList;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        List<String> employeeList = new ArrayList<>();
        this.getEmployeeList().forEach(e -> employeeList.add(e));

        return new Employees(employeeList);
    }

}
