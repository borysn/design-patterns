package patterns.creational.abstractfactory;

public class AndroidFactory implements CellPhoneAbstractFactory {

    private String ram;
    private String hdd;
    private String cpu;

    public AndroidFactory(String ram, String hdd, String cpu) {
        this.ram = ram;
        this.hdd = hdd;
        this.cpu = cpu;
    }

    @Override
    public CellPhone createCellPhone() {
        return new Android(ram, hdd, cpu);
    }
}
