package patterns.creational.abstractfactory;

public class CellPhoneFactory {

    public static CellPhone getCellPhone(CellPhoneAbstractFactory factory) {
        return factory.createCellPhone();
    }

}
