package patterns.creational.abstractfactory;

public interface CellPhoneAbstractFactory {

    CellPhone createCellPhone();

}
