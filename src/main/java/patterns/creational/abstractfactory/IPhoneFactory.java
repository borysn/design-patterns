package patterns.creational.abstractfactory;

public class IPhoneFactory implements CellPhoneAbstractFactory {

    private String ram;
    private String hdd;
    private String cpu;

    public IPhoneFactory(String ram, String hdd, String cpu) {
        this.ram = ram;
        this.hdd = hdd;
        this.cpu = cpu;
    }

    @Override
    public CellPhone createCellPhone() {
        return new IPhone(ram, hdd, cpu);
    }
}
