package patterns.creational.factory;

public enum ComputerType {
    Server("Server"),
    PC("PC");

    private String type;

    ComputerType(String type) {
        this.type = type;
    }

    public String getValue() {
        return this.type;
    }

}
