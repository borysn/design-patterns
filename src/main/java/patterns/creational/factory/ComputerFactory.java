package patterns.creational.factory;

public class ComputerFactory {

    public static Computer getComputer(ComputerType type, String ram, String hdd, String cpu) {
        if (type.getValue().equals(ComputerType.PC.getValue())) {
            return new PC(ram, hdd, cpu);
        } else if (type.getValue().equals(ComputerType.Server.getValue())) {
            return new Server(ram, hdd, cpu);
        }

        return null;
    }

}
