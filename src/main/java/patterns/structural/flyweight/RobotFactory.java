package patterns.structural.flyweight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class RobotFactory {

    private static final Logger logger = LoggerFactory.getLogger(RobotFactory.class);
    private static Map<RobotType, RobotIntf> robots = new HashMap<>();

    public RobotFactory() {
         robots = new HashMap<>();
    }

    public int getTotalRobots() {
        return this.robots.size();
    }

    public RobotIntf getRobot(RobotType type) {
        RobotIntf robot = null;

        if (robots.containsKey(type)) {
            robot = robots.get(type);
        } else {
            switch(type) {
                case LARGE:
                    logger.info("No large robot, creating one now.");
                    robot = new LargeRobot();
                    robots.put(RobotType.LARGE, robot);
                    break;
                case SMALL:
                    logger.info("No small robot, creating one now.");
                    robot = new SmallRobot();
                    robots.put(RobotType.SMALL, robot);
                    break;
                default:
                    logger.error("Robot factory can only create small and large robots.");
            }
        }

        return robot;
    }

}
