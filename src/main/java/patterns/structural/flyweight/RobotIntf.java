package patterns.structural.flyweight;

public interface RobotIntf {

    void print();

}
