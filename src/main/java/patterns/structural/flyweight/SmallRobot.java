package patterns.structural.flyweight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmallRobot implements RobotIntf {

    private static final Logger logger = LoggerFactory.getLogger(SmallRobot.class);

    @Override
    public void print() {
        logger.info("This is a small robot");
    }
}
