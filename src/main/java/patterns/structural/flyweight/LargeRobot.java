package patterns.structural.flyweight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LargeRobot implements RobotIntf {

    private static final Logger logger = LoggerFactory.getLogger(LargeRobot.class);

    @Override
    public void print() {
        logger.info("I am a large robot");
    }
}
