package patterns.structural.flyweight;

public enum RobotType {
    NANO("nano"),
    SMALL("small"),
    LARGE("large");

    private String type;

    RobotType(String type) {
        this.type = type;
    }

    public String value() {
        return this.type;
    }

}
