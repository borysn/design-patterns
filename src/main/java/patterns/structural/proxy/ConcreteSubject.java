package patterns.structural.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConcreteSubject extends Subject {

    private static final Logger logger = LoggerFactory.getLogger(ConcreteSubject.class);

    @Override
    public void doStuff() {
        logger.info("doStuff | ConcreteSubject");
    }
}
