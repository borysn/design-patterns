package patterns.structural.proxy;

public class Proxy extends Subject {

    private ConcreteSubject concreteSubject;

    public Proxy() {
        this.concreteSubject = new ConcreteSubject();
    }

    @Override
    public void doStuff() {
        this.concreteSubject.doStuff();
    }
}
