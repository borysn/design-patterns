package patterns.structural.composite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Triangle implements Shape {

    private static final Logger logger = LoggerFactory.getLogger(Triangle.class);

    @Override
    public void draw(String fillColor) {
        logger.info("Drawing Hexagon with color:" + fillColor);
    }

}
