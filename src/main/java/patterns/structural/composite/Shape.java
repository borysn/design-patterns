package patterns.structural.composite;

public interface Shape {

    void draw(String fillColor);

}
