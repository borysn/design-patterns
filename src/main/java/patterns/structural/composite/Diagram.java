package patterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class Diagram implements Shape {

    private List<Shape> shapes;

    public Diagram() {
        this.shapes = new ArrayList<>();
    }

    @Override
    public void draw(String fillColor) {
        shapes.forEach(s -> s.draw(fillColor));
    }

    public void add(Shape s) {
        shapes.add(s);
    }

    public void remove(Shape s) {
        shapes.add(s);
    }

    public void clear() {
        shapes.clear();
    }
}
