package patterns.structural.composite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Circle implements Shape {

    private static final Logger logger = LoggerFactory.getLogger(Shape.class);

    @Override
    public void draw(String fillColor) {
        logger.info("Drawing Circle with color:" + fillColor);
    }
}
