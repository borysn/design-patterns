package patterns.structural.bridge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Hexagon extends Shape {

    private static final Logger logger = LoggerFactory.getLogger(Hexagon.class);

    public Hexagon(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        logger.info("Hexagon filled with color");
        color.applyColor();
    }
}
