package patterns.structural.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LuxuryCar extends CarDecorator {

    private static final Logger logger = LoggerFactory.getLogger(LuxuryCar.class);

    public LuxuryCar(Car car) {
        super(car);
    }

    @Override
    public void assemble() {
        super.assemble();
        logger.info("assemble() | LuxuryCar");
    }
}
