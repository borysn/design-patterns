package patterns.structural.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SportsCar extends CarDecorator {

    private static final Logger logger = LoggerFactory.getLogger(CarDecorator.class);

    public SportsCar(Car car) {
        super(car);
    }

    @Override
    public void assemble() {
        super.assemble();
        logger.info("assemble() | SportsCar");
    }
}
