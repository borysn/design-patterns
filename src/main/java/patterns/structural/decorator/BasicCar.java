package patterns.structural.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicCar implements Car {

    private static final Logger logger = LoggerFactory.getLogger(Car.class);

    @Override
    public void assemble() {
        logger.info("assemble() | BasicCar");
    }
}
