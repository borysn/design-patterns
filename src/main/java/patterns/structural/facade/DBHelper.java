package patterns.structural.facade;

import java.sql.Connection;

public abstract class DBHelper {

    public abstract Connection getDBConnection();
    public abstract void generatePDFReport(String tableName, Connection con);
    public abstract void generateHTMLReport(String tableName, Connection con);

}
