package patterns.structural.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;

public class OracleHelper extends DBHelper {

    private static final Logger logger = LoggerFactory.getLogger(OracleHelper.class);

    @Override
    public Connection getDBConnection() {
        return null;
    }

    @Override
    public void generatePDFReport(String tableName, Connection con) {
        logger.info("genearting oracle pdf report");
    }

    @Override
    public void generateHTMLReport(String tableName, Connection con) {
        logger.info("generating oracle html report");
    }
}
