package patterns.structural.facade;

import java.sql.Connection;

public class HelperFacade {

    public static void generateReport(DBTypes dbType, ReportTypes reportType, String tableName) {
        DBHelper dbHelper = null;

        switch (dbType) {
            case MYSQL:
                dbHelper = new MysqlHelper();
                break;
            case ORACLE:
                dbHelper = new OracleHelper();
                break;
        }

        Connection connection = dbHelper.getDBConnection();

        switch (reportType) {
            case HTML:
                dbHelper.generateHTMLReport(tableName, connection);
                break;
            case PDF:
                dbHelper.generatePDFReport(tableName, connection);
                break;
        }
    }

}
