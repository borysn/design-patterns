package patterns.creational;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import patterns.creational.abstractfactory.AbstractFactoryTest;
import patterns.creational.builder.BuilderTest;
import patterns.creational.factory.FactoryTest;
import patterns.creational.prototype.PrototypeTest;
import patterns.creational.singleton.SingletonTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AbstractFactoryTest.class, BuilderTest.class, FactoryTest.class,
        PrototypeTest.class, SingletonTest.class
})
public class CreationalTests {
}
