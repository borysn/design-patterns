package patterns.creational.prototype;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PrototypeTest {

    private static final Logger logger = LoggerFactory.getLogger(PrototypeTest.class);

    @Test
    public void testCreation() throws CloneNotSupportedException {
        Employees employees = new Employees();
        employees.loadData();

        // clones
        Employees clone1 = (Employees) employees.clone();
        Employees clone2 = (Employees) employees.clone();

        List<String> list1 = clone1.getEmployeeList();
        List<String> list2 = clone2.getEmployeeList();

        list1.remove("Roman");
        list2.add("G'Mah");

        logger.info("employees: " + employees.getEmployeeList());
        logger.info("employees clone1: " + list1);
        logger.info("employees clone2: " + list2);


        Assert.assertTrue("new instance", !employees.equals(clone1));
        Assert.assertTrue("new instance", !employees.equals(clone2));
    }

}
