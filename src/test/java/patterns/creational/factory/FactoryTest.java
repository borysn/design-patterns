package patterns.creational.factory;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FactoryTest {

    private static final Logger logger = LoggerFactory.getLogger(FactoryTest.class);

    @Test
    public void testCreation() {
        Computer pc = ComputerFactory.getComputer(
                ComputerType.PC, "2GB", "500GB", "2.4GHz");
        Computer server = ComputerFactory.getComputer(
                ComputerType.Server, "16GB", "1TB", "2.9GHz");

        logger.info("PC: " + pc);
        logger.info("Server: " + server);

        Assert.assertEquals("is pc", true, pc.getClass().equals(PC.class));
        Assert.assertEquals("is server", true, server.getClass().equals(Server.class));
    }

}
