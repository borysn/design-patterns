package patterns.creational.builder;

import org.junit.Assert;
import org.junit.Test;

public class BuilderTest {

    @Test
    public void testCreation() {
        Vehicle vehicle = new Vehicle.VehicleBuilder(4, 4, "v4")
                .setHasAutomaticLocks(true)
                .setHasBlueTooth(true)
                .build();

        Assert.assertTrue("has bluetooth", vehicle.hasBlueTooth());
        Assert.assertTrue("has automatic locks", vehicle.hasAutomaticLocks());
    }

}
