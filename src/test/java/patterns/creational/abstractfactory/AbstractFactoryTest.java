package patterns.creational.abstractfactory;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractFactoryTest {

    private static final Logger logger = LoggerFactory.getLogger(AbstractFactoryTest.class);

    @Test
    public void testCreation() {

        CellPhone android = CellPhoneFactory.getCellPhone(
                new AndroidFactory("1GB", "16GB", "1.4GHz"));

        CellPhone iphone = CellPhoneFactory.getCellPhone(
                new IPhoneFactory("1GB", "64GB", "1.2GHz"));

        logger.info("Android: " + android);
        logger.info("IPhone: " + iphone);

        Assert.assertTrue("instance of android", android.getClass().equals(Android.class));
        Assert.assertTrue("instance of iphone", iphone.getClass().equals(IPhone.class));
    }

}
