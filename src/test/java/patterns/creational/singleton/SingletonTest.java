package patterns.creational.singleton;

import org.junit.Assert;
import org.junit.Test;

public class SingletonTest {

    @Test
    public void testSingletonCreation() {
        Singleton s1 = Singleton.getSingleton();
        Singleton s2 = Singleton.getSingleton();

        Assert.assertEquals("Same instance", true, s1.equals(s2));
    }

}
