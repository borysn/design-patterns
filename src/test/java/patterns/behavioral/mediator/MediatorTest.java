package patterns.behavioral.mediator;

import org.junit.Test;

public class MediatorTest {

    @Test
    public void testCreation() {
        ChatMediator chatMediator = new ChatMediator();

        User user1 = new User(chatMediator, "user1");
        User user2 = new User(chatMediator, "user2");
        User user3 = new User(chatMediator, "user3");
        User user4 = new User(chatMediator, "user4");

        chatMediator.addUser(user1);
        chatMediator.addUser(user2);
        chatMediator.addUser(user3);
        chatMediator.addUser(user4);

        user1.send("Hello, world!");
        user2.send("Hello, all!");
        user3.send("Hello, fam!");
        user4.send("Goodbye, cruel world!");
    }

}
