package patterns.behavioral;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import patterns.behavioral.chainofresponsibility.ChainOfResponsibilityTest;
import patterns.behavioral.command.CommandTest;
import patterns.behavioral.interpreter.InterpreterTest;
import patterns.behavioral.iterator.IteratorTest;
import patterns.behavioral.mediator.MediatorTest;
import patterns.behavioral.memento.MementoTest;
import patterns.behavioral.observer.ObserverTest;
import patterns.behavioral.state.StateTest;
import patterns.behavioral.strategy.StrategyTest;
import patterns.behavioral.templatemethod.TemplateMethodTest;
import patterns.behavioral.visitor.VisitorTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CommandTest.class, ObserverTest.class, StrategyTest.class,
        TemplateMethodTest.class, MediatorTest.class, ChainOfResponsibilityTest.class,
        StateTest.class, VisitorTest.class, InterpreterTest.class,
        IteratorTest.class, MementoTest.class
})
public class BehavioralTests {
}
