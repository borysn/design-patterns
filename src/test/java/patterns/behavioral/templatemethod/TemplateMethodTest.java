package patterns.behavioral.templatemethod;

import org.junit.Test;

public class TemplateMethodTest {

    @Test
    public void testCreation() {
        BasicDrink b1 = new Tea();
        BasicDrink b2 = new Coffee();

        b1.makeDrink();
        b2.makeDrink();
    }

}
