package patterns.behavioral.chainofresponsibility;

import org.junit.Test;

public class ChainOfResponsibilityTest {

    @Test
    public void testCreation() {
        DispenseChain dispenseChain1 = new FiftyDollarDispenser();
        DispenseChain dispenseChain2 = new TwentyDollarDispenser();
        DispenseChain dispenseChain3 = new TenDollarDispenser();

        dispenseChain1.setNextChain(dispenseChain2);
        dispenseChain2.setNextChain(dispenseChain3);

        dispenseChain1.dispense(new Currency(50));
        dispenseChain1.dispense(new Currency(20));
        dispenseChain1.dispense(new Currency(10));
        dispenseChain1.dispense(new Currency(150));
        dispenseChain1.dispense(new Currency(530));
        dispenseChain1.dispense(new Currency(1270));
    }

}
