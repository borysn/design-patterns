package patterns.behavioral.state;

import org.junit.Assert;
import org.junit.Test;

public class StateTest {

    @Test
    public void testCreation() {
        TVContext context = new TVContext();
        State tvOnState = new TVOnState();
        State tvOffState = new TVOffState();

        context.setState(tvOnState);
        context.doAction();

        Assert.assertTrue("tv on state", context.getState().getClass().equals(TVOnState.class));

        context.setState(tvOffState);
        context.doAction();

        Assert.assertTrue("tv off state", context.getState().getClass().equals(TVOffState.class));
    }

}
