package patterns.behavioral.strategy;

import org.junit.Test;

public class StrategyTest {

    @Test
    public void testCreation() {
        Game game = new Game();
        game.start();
    }

}
