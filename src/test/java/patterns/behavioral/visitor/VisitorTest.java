package patterns.behavioral.visitor;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class VisitorTest {

    private static final Logger logger = LoggerFactory.getLogger(VisitorTest.class);

    @Test
    public void testCreation() {

        List<ItemElement> items = new ArrayList<>();
        items.add(new Book(100, "789"));
        items.add(new Book(40, "456"));
        items.add(new Fruit(10, 2, "Banana"));
        items.add(new Fruit(5, 5, "Apple"));

        ShoppingCartVisitor visitor = new ShoppingCartVisitor();
        int sum = 0;

        for (ItemElement item : items) {
            sum += item.accept(visitor);
        }

        Assert.assertEquals("total should be 180", 180, sum);
    }

}
