package patterns.behavioral.memento;

import org.junit.Assert;
import org.junit.Test;
public class MementoTest {

    @Test
    public void testCreation() {

        FileWriterCaretaker fileWriterCaretaker = new FileWriterCaretaker();
        FileWriterUtil fileWriterUtil = new FileWriterUtil("data.txt");

        String str1 = "This is the first line\n";
        String str2 = "This is the second line\n";

        fileWriterUtil.write(str1);
        fileWriterCaretaker.save(fileWriterUtil);

        fileWriterUtil.write(str2);
        fileWriterCaretaker.undo(fileWriterUtil);

        Assert.assertEquals("should only have first line", str1, fileWriterUtil.toString());

    }

}
