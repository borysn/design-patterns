package patterns.behavioral.observer;

import org.junit.Test;

public class ObserverTest {

    @Test
    public void testObserverUpdate() {
        Observer o1 = new Observer();
        Observer o2 = new Observer();
        Subject s1 = new Subject();

        s1.register(o1);
        s1.setData(2);
        s1.register(o2);
        s1.setData(3);
        s1.unregister(o1);
        s1.setData(4);
        s1.unregister(o2);
        s1.setData(5);
    }

}
