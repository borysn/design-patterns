package patterns.behavioral.command;

import org.junit.Test;

public class CommandTest {

    @Test
    public void testCreation() {
        FileSystemReceiver fileSystemReceiver = FileSystemReceiverUtil.getUnderlyingFileSystem();
        OpenFileCommand openFileCommand = new OpenFileCommand(fileSystemReceiver);
        WriteFileCommand writeFileCommand = new WriteFileCommand(fileSystemReceiver);
        CloseFileCommand closeFileCommand = new CloseFileCommand(fileSystemReceiver);

        FileInvoker fileInvoker = new FileInvoker(openFileCommand);
        fileInvoker.execute();

        fileInvoker = new FileInvoker(writeFileCommand);
        fileInvoker.execute();

        fileInvoker = new FileInvoker(closeFileCommand);
        fileInvoker.execute();
    }

}
