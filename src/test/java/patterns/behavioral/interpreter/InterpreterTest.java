package patterns.behavioral.interpreter;

import org.junit.Assert;
import org.junit.Test;

public class InterpreterTest {

    @Test
    public void testCreation() {

        String expression1 = "28 in Binary";
        String expression2 = "28 in Hexadecimal";

        Interpreter interpreter = new Interpreter(new InterpreterContext());

        Assert.assertEquals("should be 11100", "11100",
                interpreter.interpret(expression1));

        Assert.assertEquals("should be 1c", "1c",
                interpreter.interpret(expression2));
    }

}
