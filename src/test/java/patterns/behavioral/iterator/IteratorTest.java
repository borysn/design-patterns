package patterns.behavioral.iterator;

import org.junit.Assert;
import org.junit.Test;

public class IteratorTest {

    @Test
    public void testCreation() {
        ChannelsIntf channels = new Channels();

        Channel c1 = new Channel(98.5, ChannelType.ENGLISH);
        Channel c2 = new Channel(99.5, ChannelType.ENGLISH);
        Channel c3 = new Channel(100.5, ChannelType.ENGLISH);
        Channel c4 = new Channel(101.5, ChannelType.FRENCH);
        Channel c5 = new Channel(102.5, ChannelType.FRENCH);
        Channel c6 = new Channel(103.5, ChannelType.FRENCH);
        Channel c7 = new Channel(104.5, ChannelType.HINDI);
        Channel c8 = new Channel(105.5, ChannelType.HINDI);
        Channel c9 = new Channel(106.5, ChannelType.HINDI);

        channels.addChannel(c1);
        channels.addChannel(c2);
        channels.addChannel(c3);
        channels.addChannel(c4);
        channels.addChannel(c5);
        channels.addChannel(c6);
        channels.addChannel(c7);
        channels.addChannel(c8);
        channels.addChannel(c9);

        ChannelIteratorIntf iteratorEnglish = channels.iterator(ChannelType.ENGLISH);
        while (iteratorEnglish.hasNext()) {
            Channel c = iteratorEnglish.next();
            Assert.assertEquals("should be ENGLISH", ChannelType.ENGLISH,
                    c.getType());
        }

        ChannelIteratorIntf iteratorFrench = channels.iterator(ChannelType.FRENCH);
        while (iteratorFrench.hasNext()) {
            Channel c = iteratorFrench.next();
            Assert.assertEquals("should be FRENCH", ChannelType.FRENCH,
                    c.getType());
        }

        ChannelIteratorIntf iteratorHindi = channels.iterator(ChannelType.HINDI);
        while (iteratorHindi.hasNext()) {
            Channel c = iteratorHindi.next();
            Assert.assertEquals("should be HINDI", ChannelType.HINDI,
                    c.getType());
        }

        ChannelIteratorIntf iteratorAll = channels.iterator(ChannelType.ALL);
        while (iteratorAll.hasNext()) {
            Channel c = iteratorAll.next();
            ChannelType type = c.getType();

            Assert.assertTrue("should be ENGLISH || FRENCH || HINDI",
                    type.equals(ChannelType.ENGLISH) ||
                            type.equals(ChannelType.FRENCH) ||
                            type.equals(ChannelType.HINDI));
        }
    }

}
