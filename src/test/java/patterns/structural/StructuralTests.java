package patterns.structural;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import patterns.structural.adapter.AdapterTest;
import patterns.structural.bridge.BridgeTest;
import patterns.structural.composite.CompositeTest;
import patterns.structural.decorator.DecoratorTest;
import patterns.structural.facade.FacadeTest;
import patterns.structural.flyweight.FlyweightTest;
import patterns.structural.proxy.ProxyTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AdapterTest.class, CompositeTest.class, DecoratorTest.class,
        ProxyTest.class, FlyweightTest.class, FacadeTest.class,
        BridgeTest.class
})
public class StructuralTests {
}
