package patterns.structural.composite;

import org.junit.Test;

public class CompositeTest {

    @Test
    public void testCreation() {
        Shape s1 = new Circle();
        Shape s2 = new Circle();
        Shape s3 = new Triangle();

        Diagram diagram = new Diagram();
        diagram.add(s1);
        diagram.add(s2);
        diagram.add(s3);

        diagram.draw("Hazel");
    }

}
