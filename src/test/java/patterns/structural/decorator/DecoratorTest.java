package patterns.structural.decorator;

import org.junit.Test;

public class DecoratorTest {

    @Test
    public void testCarCreation() {
        Car car1 = new BasicCar();
        Car car2 = new BasicCar();
        CarDecorator carDecorator1 = new LuxuryCar(car1);
        CarDecorator carDecorator2 = new SportsCar(car2);

        carDecorator1.assemble();
        carDecorator2.assemble();
    }

}
