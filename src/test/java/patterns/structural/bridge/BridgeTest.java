package patterns.structural.bridge;

import org.junit.Test;

public class BridgeTest {

    @Test
    public void testCreation() {
        Shape s1 = new Hexagon(new GreenColor());
        s1.applyColor();

        Shape s2 = new Pentagon(new RedColor());
        s2.applyColor();
    }

}
