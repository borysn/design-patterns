package patterns.structural.facade;

import org.junit.Test;

public class FacadeTest {

    @Test
    public void testCreation() {
        String tableName = "employee";
        HelperFacade.generateReport(DBTypes.MYSQL, ReportTypes.HTML, tableName);
        HelperFacade.generateReport(DBTypes.ORACLE, ReportTypes.PDF, tableName);
        HelperFacade.generateReport(DBTypes.MYSQL, ReportTypes.PDF, tableName);
        HelperFacade.generateReport(DBTypes.ORACLE, ReportTypes.HTML, tableName);
    }

}
