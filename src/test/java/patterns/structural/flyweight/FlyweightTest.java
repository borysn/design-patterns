package patterns.structural.flyweight;

import org.junit.Assert;
import org.junit.Test;

public class FlyweightTest {

    @Test
    public void testCreation() {
        RobotFactory robotFactory = new RobotFactory();
        RobotIntf r1 = robotFactory.getRobot(RobotType.LARGE);
        RobotIntf r2 = robotFactory.getRobot(RobotType.SMALL);
        RobotIntf r3 = robotFactory.getRobot(RobotType.NANO);

        r1.print();
        r2.print();

        Assert.assertEquals("nano robot should be null", null, r3);

        for (int i = 0; i < 5; i++) {
            RobotIntf r4 = robotFactory.getRobot(RobotType.LARGE);
            r4.print();
            Assert.assertEquals("single instance of large robot", true, r1.equals(r4));
        }

        for (int i = 0; i < 5; i++) {
            RobotIntf r5 = robotFactory.getRobot(RobotType.SMALL);
            r5.print();
            Assert.assertEquals("single instance of small robot", true, r2.equals(r5));
        }
    }

}
