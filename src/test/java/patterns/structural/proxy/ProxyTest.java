package patterns.structural.proxy;

import org.junit.Test;

public class ProxyTest {

    @Test
    public void testProxyCreation() {
        Proxy proxy = new Proxy();
        proxy.doStuff();
    }

}
