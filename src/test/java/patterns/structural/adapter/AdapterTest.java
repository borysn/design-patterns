package patterns.structural.adapter;

import org.junit.Assert;
import org.junit.Test;

public class AdapterTest {

    @Test
    public void testVoltCreation() {
        SocketAdapter socketAdapter = new SocketAdapterImpl();
        Volt v1 = socketAdapter.get120Volt();
        Volt v2 = socketAdapter.get12Volt();
        Volt v3 = socketAdapter.get3Volt();

        Assert.assertEquals("120 volt", 120, v1.getVolts());
        Assert.assertEquals("12 volt", 12, v2.getVolts());
        Assert.assertEquals("3 volt", 3, v3.getVolts());
    }

}
