package patterns;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import patterns.behavioral.BehavioralTests;
import patterns.creational.CreationalTests;
import patterns.structural.StructuralTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        BehavioralTests.class, CreationalTests.class, StructuralTests.class
})
public class AllTests {
}
